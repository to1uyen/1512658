import {
    StyleSheet
} from 'react-native';
const  styles=StyleSheet.create({
    CustomItem: {
        flex:7
    },
    waperaddMon:{
        flex:3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'white'
    },
    TiTleText:{
        flex:1,
        fontWeight: 'bold',
        color:'black',
        fontSize:17
    },
    GiaThat:{
        color:'#898989',
        textDecorationLine: 'line-through',
        fontSize:15
    },
    GiaKhuyenMai:{
        fontSize:15,
        color: 'black'
    },
    Textinput:{
        fontSize: 15,
        backgroundColor: 'white',
        textAlign: 'center',
        borderWidth: 1,
        borderColor:'#898989',
        width: 40,
        height: 40,
    },
    image:{
        width:90,
        height:90,
        marginTop: 5,
        marginLeft: 10,
        marginBottom: 5
    },
    sectionHeader:{
        fontSize: 17,
        fontWeight: 'bold',
        backgroundColor: 'rgba(247,247,247,1.0)',
        height:40,
        textAlignVertical: 'center',
        marginLeft: 10,
        textTransform: 'uppercase'
    }
});
export default styles;