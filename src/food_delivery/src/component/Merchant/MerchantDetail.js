import React,{Component} from 'react';
import {connect} from 'react-redux'
import {storagefooditem, saveprice, savequantity} from '../.././Actions'
import {
    Image,
    View,
    TouchableOpacity,
    Dimensions, AsyncStorage, FlatList, Text, TextInput, WebView
} from 'react-native';
import styles from './styleMerchantDetail';
import {Actions} from "react-native-router-flux";
import axios from "axios";
import Swiper from 'react-native-swiper';
import VideoComponent from "../Video/VideoComponent";
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import dateFormat from 'dateformat'
const  {height: HEIGHT} = Dimensions.get('window');
const w =Dimensions.get('window').width;

type Props = {};

 class MerchantDetail extends Component<Props> {
    constructor(props){
        super(props);
        this.state={
            erorr:'',
            check: 0,
            data:[],
            adress:'',
            restaurant:{},
            databasket:{},
            resource:[],
            image:null,
            isFirstrough:true,
            textcolorfirst:'#DB7F67',
            textcolorsecond:'black',
            comment:{},
            contentcomment:''

        }
          this.handlegetMenuRestaurant();
        this.getComment()
    }

    handlegetMenuRestaurant = async ()=>{
        var self=this;
        var idRestautant=await AsyncStorage.getItem('idRestaurant')
        const url='http://food-delivery-server.herokuapp.com/restaurant/getMenu/'+ idRestautant;
        axios.get(url)
            .then(res =>{
                self.setState({data:res.data.menu, image:res.data.restaurant.image, adress:res.data.address.address,
                    restaurant:res.data.restaurant, resource:res.data.resource })
            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1});
                }
            });
    }

    getComment = async ()=>{
        var self=this;
        var idRestautant=await AsyncStorage.getItem('idRestaurant')
        const url='http://food-delivery-server.herokuapp.com/restaurant/getAllCommentById/'+ idRestautant;
        axios.get(url)
            .then(res =>{
                self.setState({comment:res.data})
            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1});
                }
            });
    }
     handleSendComment(){
        var self=this;
        const url='http://food-delivery-server.herokuapp.com/restaurant/addComment';
        const param={
            name: this.props.info.userName,
            idRestaurant:this.state.restaurant.id,
            content: this.state.contentcomment
        }
        axios.post(url, param)
            .then(response => {
                self.setState({contentcomment:''})
                this.getComment()
            })
            .catch((error) => {
                self.setState({contentcomment:''})
                this.getComment()
                // alert('loi')
                // self.setState({erorr:error})
            });
    }
    _storageData = async (item)=> {
        try {
            await AsyncStorage.setItem('itemFood', JSON.stringify(item));
        } catch (e) {
            alert(e.toString());
        }
    }

    saveItem(item){
        const { storagefooditem, saveprice, savequantity} = this.props;
        var totalprice = this.props.price + item.price;
        var totalquantity = this.props.quantity + 1
        {storagefooditem(item)}
        {savequantity(totalquantity)}
        {saveprice(totalprice)}
    }

    renderPage(item,index){
        return(
            <View style={{position:'absolute'}}>
                {item.type == 'image' ?
                    <View style={{width:'100%'}}>
                        <Image source={{uri:item.url}}style={{width:w, height:170,  position: 'absolute', resizeMode:'cover' }}></Image>
                        <TouchableOpacity onPress={()=>Actions.pop()}>
                            <Image source={require('../img/back24.png')} style={{marginTop:60}}></Image>
                        </TouchableOpacity>
                        <View style ={{backgroundColor:'black', position:'absolute', marginTop:130, width:'100%', opacity:0.3, height:40}}>
                        </View>
                        <Text style={{color:'white', marginTop:45, marginLeft: 5}}>{this.state.restaurant.name}</Text>
                        <Text style ={{color:'white', marginLeft:5}}>{this.state.adress}</Text>
                    </View>


                    :<VideoComponent url={item.url}/>
                }

            </View>
        )
    }

     FirstRoute (){
       this.setState({isFirstrough:true,textcolorfirst:'#DB7F67', textcolorsecond:'black'})
     };
    SecondRoute() {
        this.setState({isFirstrough:false, textcolorsecond:'#DB7F67', textcolorfirst:'black'})
    }

    render(){
        const pagesCount = this.state.resource.length;
        const pages = this.state.resource.map((item, index) => {
            return this.renderPage(item, index);
        });
        return(
            <View style={{ height:HEIGHT,
                width: '100%'}}>
                <View style={{height:170, width:w}}>
                    <Swiper height={170}
                            loop={false}
                            autoplay={false}
                            showsPagination={false}
                            key={pagesCount}>
                        {this.state.resource.map((item,index)=>
                        item.type==='image' ? <View style={{width:'100%'}}>
                            <Image source={{uri:item.url}}style={{width:w, height:170,  position: 'absolute', resizeMode:'cover' }}></Image>
                            <TouchableOpacity onPress={()=>Actions.pop()}>
                                <Image source={require('../img/back24.png')} style={{marginTop:60}}></Image>
                            </TouchableOpacity>
                            <View style ={{backgroundColor:'black', position:'absolute', marginTop:130, width:'100%', opacity:0.3, height:40}}>
                            </View>
                            <Text style={{color:'white', marginTop:45, marginLeft: 5}}>{this.state.restaurant.name}</Text>
                            <Text style ={{color:'white', marginLeft:5}}>{this.state.adress}</Text>
                        </View> :
                            <View style={{width:'100%'}}>
                                <WebView key={index} source={{uri:item.url+'?rel=0&autoplay=0&showinfo=0&controls=0'}}
                                         style={{width:w, height:170,  position: 'absolute'}}
                                />
                                <TouchableOpacity onPress={()=>Actions.pop()}>
                                    <Image source={require('../img/back24.png')} style={{marginTop:60}}></Image>
                                </TouchableOpacity>
                            </View>
                            )}
                    </Swiper>
                </View>
                <View style={{height:HEIGHT-170}}>
                   <View style ={{flexDirection:'row', height:40, alignItems:'center'}}>
                       <TouchableOpacity onPress={()=>this.FirstRoute()} style={{flex:1}}>
                           <Text style={{color:this.state.textcolorfirst, textAlign: 'center'}}>Món</Text>
                        </TouchableOpacity>
                       <View style={{width:3, backgroundColor:'white', height:40}}></View>
                       <TouchableOpacity onPress={()=>this.SecondRoute()} style={{flex:1}}>
                           <Text style={{color:this.state.textcolorsecond, textAlign:'center'}}>Bình luận</Text>
                       </TouchableOpacity>
                   </View>
                    <View style={{height:2, width:w, backgroundColor:'#A0A0A0'}}></View>
                    {
                        this.state.isFirstrough &&
                            <View>
                                <View style={{height:HEIGHT-265}}>
                                    <FlatList
                                        data={this.state.data}
                                        renderItem={this._renderItemMenu}
                                        keyExtractor = { (item, index) => index.toString() }
                                    />
                                </View>
                                <View style={{height:50, blurRadius:1,backgroundColor: 'rgba(153,209,211,0.5)', flexDirection:'row', alignItems:'center'}}>
                                    <TouchableOpacity style={{flexDirection:'row'}} onPress={()=>{Actions.Basket()}}>
                                        <Text style={{flex:1}}>Giỏ hàng</Text>
                                        <Text style={{flex:1}}>Tổng Tiền: {this.props.price}</Text>
                                        <Text style={{flex:1}}>Số lượng: {this.props.quantity}</Text>

                                    </TouchableOpacity>
                                </View>
                            </View>

                    }
                    {
                        !this.state.isFirstrough &&
                            <View>
                                <View style={{height:80}}>
                                    <View style={{height:78}}>
                                        <Text style={{color:'black', marginLeft:5, flex:1.3}}>{this.props.info.userName}</Text>
                                        <View  style={{flex:2, margin:5, backgroundColor:'white', flexDirection:'row',
                                        justifyContent: 'center', alignItems:'center'}}>
                                            <TextInput style={{flex:4.5}} placeholder={'Nhập bình luận'}
                                                       onChangeText={(contentcomment) => this.setState({contentcomment})}
                                            ></TextInput>
                                            <TouchableOpacity style={{flex:0.5}} onPress={()=>this.handleSendComment()}>
                                                <Image source={require('../img/send.png')} style={{width:25, height:25}}/>
                                            </TouchableOpacity>
                                        </View>

                                    </View>
                                    <View style={{height:2, backgroundColor:'#A0A0A0'}}></View>
                                </View>

                                <View  style={{height:HEIGHT-300}}>
                                    <FlatList
                                        data={this.state.comment.result}
                                        renderItem={this._renderItemComment}
                                        keyExtractor = { (item) => item.id.toString()}
                                    />
                                </View>
                            </View>

                    }
                </View>


            </View>
        );
    }
     _renderItemComment=({item})=>(
         <View>
             <View style={{flexDirection:'row', justifyContent: 'center', alignItems:'center', backgroundColor:'white'}}>
                 <Text style={{fontWeight: 'bold', flex:1}}>{item.name}</Text>
                 <View style={{marginLeft:10, backgroundColor:'#DB7F67', borderRadius:10, flex:3, margin: 5}}>
                     <Text style={{margin:5}}>{item.content}</Text>
                     <Text style={{textAlign: 'right', margin:5}}>{dateFormat(item.createAt, "dd/mm/yyyy, h:MM:ss TT")}</Text>
                 </View>
             </View>
             <View style={{width:w, height:2, backgroundColor:'#A0A0A0'}}></View>
         </View>

     )
    _renderItemMenu=({item})=>(
        <View style={styles.CustomItem}>
            <View style={styles.waperaddMon}>
                <View style={{flex:1}}>
                    {item.image!="" ? <Image source={{uri:item.image}} style={styles.image}></Image> : <Image source={require('../img/milktea.png')} style={styles.image}></Image>}
                </View>

                <View style={{flex:3, marginLeft: 20, marginBottom: 10}} >
                    <Text style={styles.TiTleText}>{item.name}</Text>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <View style={{flexDirection: 'row', flex:1.5}}>
                            <Text style={styles.GiaKhuyenMai}>Giá: </Text>
                            <Text style={styles.GiaKhuyenMai}>{item.price}</Text>
                        </View>
                        <View style={{ flex:1, alignItems: 'center'}}>
                            <TouchableOpacity onPress={()=>this.saveItem(item)} >
                                <Image source={require('../img/plus.png')} style={{width:50, height:50}}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <Text style={styles.GiaKhuyenMai}>Đã bán: </Text>
                        <Text style={styles.GiaKhuyenMai}>{item.sold}</Text>
                    </View>
                </View>
            </View>
            <View style={{backgroundColor:'#A0A0A0', height:2}}>
            </View>
        </View>
    )

}

const mapStateToProps = (state) => ( {
    data: state.appReducer.data,
    price:state.appReducer.price,
    quantity:state.appReducer.quantity,
    info:state.appReducer.info,
    user:state.appReducer.user
})

const mapDispatchToProps = (dispatch) => ({
    storagefooditem: (item) => { dispatch(storagefooditem(item)) },
    saveprice:(price) =>{dispatch(saveprice(price))},
    savequantity:(quantity) => {dispatch(savequantity(quantity))}
})

export default connect( mapStateToProps, mapDispatchToProps )(MerchantDetail)