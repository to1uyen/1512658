import {
    StyleSheet
} from 'react-native';
const  styles=StyleSheet.create({
    waperCustomMerchant:{
        marginTop:5,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor:'white'

    },
    waperSearch:{
        flex: 1,
        flexDirection: 'row',
        alignItems:'center',
    },
    TextInput:{
        width:'85%',
        backgroundColor:'#DB7F67',
        height:40,
        marginLeft:10,
        fontSize:17
    },
    wapermerchant:{
        flex: 9,
    },
    TiTleText:{
        fontWeight: 'bold',
        fontSize:15,
        marginLeft:5,
        color:'black',

    },
    AddressText:{
      flex:1,marginLeft:10,
      fontSize:17,
        color: 'black'
    },
    waperAddress:{
      flexDirection:'row',
      alignItems:'center',
        justifyContent: 'center',
        marginTop: 10,
        marginLeft:5,
        flex:1
    },
    waperPrice:{
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        flex:1
    },
    Icon:{
        width: 25,
        height: 25
    },
    waperDetail:{
        flex:3,
        width:'100%',
        height:110
    },
    image:{
        width:95,
        height:95,
        marginLeft:5
    },
    ViewType:{
        flex:2,
        flexDirection:'row',
        backgroundColor:'white',
        alignItems:'center',
        justifyContent: 'center'
    },
    text:{
        color:'black',
        marginRight: 25,
        fontSize:15,
        textAlign:'center'
    },
    text1:{
        color:'red',
        marginRight: 25,
        fontSize:15,
        textAlign:'center'
    }

});
export default styles;