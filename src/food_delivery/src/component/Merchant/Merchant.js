import React,{Component} from 'react';
import {
    Image,
    View,
    Text,
    TextInput,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator, AsyncStorage
} from 'react-native';
import styles from './styleMerchant';
import {Actions} from "react-native-router-flux";
import { Rating } from 'react-native-elements';
const  {height: HEIGHT} = Dimensions.get('window')
const {width:WIDTH} = Dimensions.get('window').width;
import axios from 'axios';


export default class Merchant extends Component{
    constructor(props){
        super(props);
        this.state ={
            data:[],
            category:[],
            erorr:'',
            check:'',
            showProgress: false,
            keysearch:'',
        }

        {this.handlegetCategory()};
        {this.handlegetRestaurant(1)};
    };
    handlegetCategory(){
        var self=this;
        const url='http://food-delivery-server.herokuapp.com/categories/getAll';
        axios.get(url)
            .then(res=>{
                self.setState({category:res.data});
            })
            .catch()
    }

    handlegetRestaurant(id){
        var self=this;
        self.setState({showProgress:true})
        const url='http://food-delivery-server.herokuapp.com/restaurant/getCategory/'+id;
        axios.get(url)
            .then(res =>{
                self.setState({data:res.data, showProgress:false})

            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1, showProgress:false});
                }
            });
    }

    handlesearchRestaurant(){
        var self=this;
        self.setState({showProgress:true})
        const url='http://food-delivery-server.herokuapp.com/restaurant/search?name='+this.state.searchkey;
        axios.get(url)
            .then(res =>{
                self.setState({data:res.data, showProgress:false})
            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1, showProgress:false});
                }
            });
    }
    _storageData = async (id)=>{
        await  AsyncStorage.setItem('idRestaurant',id.toString());
    }
    saveID(id){
        this._storageData(id);
        Actions.merchantdetail();
    }
    render(){
        return(
            <View style={{ height:HEIGHT-50,
                width: '100%'}}>
                <View style={styles.waperSearch}>
                    <TextInput placeholder={'Tìm thương hiệu'} style={styles.TextInput} onChangeText={(name)=>{this.setState({searchkey:name})}}/>
                    <TouchableOpacity style={{ marginLeft:5}} onPress={()=>this.handlesearchRestaurant()}>
                        <Image source={require('../img/search.png')} ></Image>
                    </TouchableOpacity>


                </View>
                <View style={styles.wapermerchant}>
                    <View style={{flex:1}}>
                        <View style={styles.ViewType}>
                                <FlatList
                                    data={this.state.category}
                                    renderItem={this._renderItem1}
                                    horizontal={true}
                                    keyExtractor = { (item, index) => index.toString() }
                                />

                        </View>
                        {this.state.showProgress ?<View style={{flex:9}}>
                        <ActivityIndicator animating={true} size="large" color="#008489"/>
                            </View> :<View style={{flex:8}}>
                            {this.state.data.length>0 ?<FlatList
                                data={this.state.data}
                                renderItem={this._renderItem}
                                keyExtractor = { (item) => item.id.toString()}
                            /> :<Text style={styles.text}>Không có dữ liệu</Text>}

                        </View>}
                    </View>
                </View>

            </View>

        );

    }
    _renderItem1=({item})=>(
        <View>
            <TouchableOpacity onPress={(id)=>this.handlegetRestaurant(item.id)}>
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center'}}>
                    <Image source={{uri:item.image}} style={{width:70, height:70, borderRadius:100, marginLeft:15 }}/>
                    <Text style={{marginLeft:5}}>{item.name}</Text>
                </View>

            </TouchableOpacity>
        </View>

    );
    _renderItem=({item})=>(
        <View style={styles.waperCustomMerchant}  >
            <TouchableOpacity style={styles.waperCustomMerchant} onPress={(id)=>this.saveID(item.id)}>
                <View >
                    <Image source={{uri:item.image}} style={styles.image}></Image>
                </View>
                <View style={[styles.waperDetail, {width:WIDTH -95}]}>
                    <Text style={styles.TiTleText}>{item.name}</Text>
                    <Rating
                        showRating={false}
                        readonly
                        type="star"
                        fractions={1}
                        startingValue={item.rating}
                        imageSize={20}
                        style={{flex:1, marginLeft:5 }}
                    />
                    <View style={{flexDirection: 'row', marginTop: 5, marginBottom: 5, flex:1, marginLeft:5}}>
                        <View style={styles.waperAddress}>
                            <Image source={require('../img/money.png')}  style={styles.Icon}></Image>
                            <Text style={styles.AddressText}>{item.feeShip} VND/km</Text>
                        </View>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 5, marginBottom: 5, flex:1, marginLeft:5}}>
                        <View style={styles.waperPrice}>
                            <Image source={require('../img/timeopen.png')} style={styles.Icon}></Image>
                            <Text style={styles.AddressText}>{item.timeOpen}</Text>
                        </View>
                        <View style={styles.waperPrice}>
                            <Image source={require('../img/timeclose.png')} style={styles.Icon}></Image>
                            <Text style={styles.AddressText}>{item.timeClose}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

        </View>
    )
};

