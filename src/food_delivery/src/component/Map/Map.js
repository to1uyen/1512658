import React from 'react';
import axios from 'axios'
import {
    StyleSheet,
    View,
    Dimensions, Text, Image, AsyncStorage
} from 'react-native';

import MapView, { Marker, ProviderPropType, Callout } from 'react-native-maps';
import {Actions} from "react-native-router-flux";
import _ from 'lodash'

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 10.773533;
const LONGITUDE = 106.702899;
const LATITUDE_DELTA =  0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;;


class Map extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            markers:[],
        };
        this.getRestaurantNearMeDelayed = _.debounce(this.onRegionChange, 2000);
         this.getRestaurantNearMe()
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition((position) => {
                var lat = parseFloat(position.coords.latitude)
                var long = parseFloat(position.coords.longitude)

                var initialRegion = {
                    latitude: lat,
                    longitude: long,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }
                alert(JSON.stringify(initialRegion))
                //this.setState({region: initialRegion})
            },
            (error) => alert(JSON.stringify(error)),
            {enableHighAccuracy:false, timeout: 20000, maximumAge: 1000});
    }

    getRestaurantNearMe(){
        var self=this;
        const url='http://food-delivery-server.herokuapp.com/restaurant/nearMe/'+this.state.region.latitude+'&'+this.state.region.longitude;
        axios.get(url)
            .then(res =>{
                this.setState({markers:res.data})
            })
            .catch(function (error) {

            });
    }

    _storageData = async (id)=>{
        await  AsyncStorage.setItem('idRestaurant',id.toString());
    }
    saveID(id){
        this._storageData(id);
        Actions.merchantdetail();
    }
    onRegionChange=(region)=>{
        this.setState({region });
        this.getRestaurantNearMe()
    }
    render() {
        return (
            <View style={styles.container}>
                <MapView
                    provider={this.props.provider}
                    style={styles.map}
                    initialRegion={this.state.region}
                    onRegionChange={region => {
                        this.getRestaurantNearMeDelayed(region)
                    }}
                    >
                    {this.state.markers.map(marker => (
                        <Marker
                            coordinate={{longitude: marker.longitude, latitude: marker.latitude}}
                            title={marker.RESTAURANT.name}
                            description={marker.street}
                        >
                            <Callout onPress={(id)=>this.saveID(marker.id)}>
                                <View style={{height:50}}>
                                    <Text>{marker.RESTAURANT.name}</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={{flex: 4}}>{marker.street}</Text>
                                        <Image
                                            source={{uri:marker.RESTAURANT.image}}
                                            style={{ width: 30, height: 30 , flex:1}}
                                        />
                                    </View>

                                </View>

                            </Callout>


                        </Marker>

                    ))}
                </MapView>
            </View>
        );
    }
}

Map.propTypes = {
    provider: ProviderPropType,
};

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    latlng: {
        width: 200,
        alignItems: 'stretch',
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
});

export default Map;