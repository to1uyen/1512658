import YouTube from 'react-native-youtube'
import React, {Component} from 'react';

class VideoComponent extends Component {
    render() {
        const parts = this.props.url.split('/');
        const id = parts.pop();
        alert(id)
        return (
            <YouTube
                videoId={id}   // The YouTube video ID
                play={true}             // control playback of video with true/false
                fullscreen={false}       // control whether the video should play in fullscreen or inline
                loop={true}             // control whether the video should loop when ended

                onReady={e => this.setState({ isReady: true })}
                onChangeState={e => this.setState({ status: e.state })}
                onChangeQuality={e => this.setState({ quality: e.quality })}
                onError={e => this.setState({ error: e.error })}
                apiKey={'AIzaSyAsaT1kLVZgZav18CtQoT4f9kS-wCltTns'}
                style={{ alignSelf: 'stretch', height: 170 }}
            />
        );
    }
}

export default VideoComponent;