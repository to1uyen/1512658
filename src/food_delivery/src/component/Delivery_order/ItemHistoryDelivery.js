import React, {Component} from 'react';
import {View, Text, TouchableOpacity, AsyncStorage} from 'react-native'
import {Actions} from "react-native-router-flux";
import connect from "react-redux/es/connect/connect";
import dateFormat from 'dateformat'
import {saveidhistorydelivery} from "../../Actions"

class ItemHistoryDelivery extends Component {
    handleShowMore(id){
        this.props.saveidhistorydelivery(id)
        Actions.DetailHistory()

    }
    render() {
        const {item}=this.props
        return (
            <View style={{backgroundColor:'white', marginTop: 10, alignItems:'center'}}>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                    <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Mã hóa đơn:</Text>
                    <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.id}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                    <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Tổng tiền:</Text>
                    <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.totalPrice}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                    <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Địa chỉ::</Text>
                    <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.address}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                    <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Ngày đặt:</Text>
                    <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{dateFormat(item.date, "dd/mm/yyyy, h:MM:ss TT")}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                    <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Số điện thoại:</Text>
                    <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.phone}</Text>
                </View>
                <TouchableOpacity style={{borderRadius:30, backgroundColor:'#DB7F67', width:'30%', alignItems:'center', height:30, justifyContent:'center', margin: 10}}
                onPress={(id)=>this.handleShowMore(item.id)}>
                    <Text>Xem thêm</Text>
                </TouchableOpacity>

            </View>
        );
    }
}
const mapStateToProps = (state) => ( {

})
const mapDispathToProps = (dispatch)=>({
    saveidhistorydelivery:(id)=>{dispatch(saveidhistorydelivery(id))}
})
export default connect(mapStateToProps,mapDispathToProps)(ItemHistoryDelivery);