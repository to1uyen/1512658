import {
    StyleSheet
} from 'react-native';
const  styles=StyleSheet.create({
    wapermap:{
        flex: 3,
        backgroundColor: 'white'
    },
    waperDirection:{
        flex: 2,
        marginBottom: 10,
        backgroundColor: 'white'
    },
    waperDetailMon:{
        flex:3,
        backgroundColor:'white'
    },
    waperDatHang:{
        flex:0.7
    },
    goBack:{
        flex:0.7,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor:'white',
        marginBottom: 3
    },
    waperButtonBack:{
        flex:1,
        marginLeft: 10
    },
    TextTitle:{
        flex:9,
        fontSize:20,
        fontWeight: 'bold',
        textAlign:'center',
        color:'black'
    },
    TextDatHang:{
        fontWeight: 'bold',
        fontSize:15,
        width:100
    },
    waperButtonDatHang:{
        backgroundColor:'#99D1D3',
        justifyContent: 'center',
        alignItems: 'center',
        flex:1
    }

});
export default styles;