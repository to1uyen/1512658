import React, {Component} from 'react';
import {View, Text, Dimensions, TouchableOpacity, Image, FlatList, TextInput, AsyncStorage} from 'react-native'
import connect from "react-redux/es/connect/connect";
import styles from "./Style_HistoryDeli";
import {Actions} from "react-native-router-flux";
import axios from "axios";
const  {height: HEIGHT} = Dimensions.get('window')

class DetailHistory extends Component {
    constructor(props){
        super(props)
        this.state={
            datadetail:{},
            datarestaurant:{},
            orderdeatail:[]
        }

    }
    componentDidMount(){
        this.handlegetorder();

    }
    handlegetorder= async ()=>{
        var self=this;
        self.setState({showProgress:true})
        const url='http://food-delivery-server.herokuapp.com/order/getOrder/'+this.props.idhistorydelivery;
        const AuthStr = 'Bearer '.concat(this.props.user.token);
        axios.get(url, { headers: { Authorization: AuthStr } })
            .then(res =>{
                self.setState({datadetail:res.data, showProgress:false,orderdeatail:res.data.details })
                this.handlegetMenuRestaurant()
            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1, showProgress:false});
                }
            });
    }

    handlegetnamenu(id){
            var index = this.state.datarestaurant.menu.findIndex(el => el.id == id);
            return this.state.datarestaurant.menu[index].name
    }
    handlegetMenuRestaurant (){
        var self=this;
        const url='http://food-delivery-server.herokuapp.com/restaurant/getMenu/'+ this.state.datadetail.order.idRestaurant;
        axios.get(url)
            .then(res =>{
                self.setState({orderdeatail:this.state.orderdeatail.map(item=>({...item, nameres:res.data.restaurant.name})), datarestaurant:res.data})
                self.setState({orderdeatail:this.state.orderdeatail.map(item=>({...item, namemenu:this.handlegetnamenu(item.idFood)}))})
                // const name = this.handlegetnamenu(301)

            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1});
                }
            });
    }
    render() {
        return (
            <View style={{height:HEIGHT,
                width: '100%', backgroundColor:'#E8E8E8'}}>

                <View style={{flex: 1}}>
                    <View style={styles.HeaderGioHang}>
                        <TouchableOpacity style={styles.waperButtonclose} onPress={()=>Actions.pop()}>
                            <Image source={require('../img/Back.png')}/>
                        </TouchableOpacity>
                        <Text style={styles.TextGioHang}>Chi Tiết</Text>
                    </View>
                </View>
              <View style={{flex:9}}>
                  {this.state.datarestaurant && <FlatList
                      data={this.state.orderdeatail}
                      renderItem={this._renderItem}
                      keyExtractor = { (item, index) => index.toString() }
                  />}

              </View>
            </View>

        );
    }
    _renderItem=({item})=>(
        <View style={{ marginBottom:10, backgroundColor: 'white'}}>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Mã hóa đơn:</Text>
                <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.idOrder}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Nhà hàng:</Text>
                <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.nameres}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Đồ ăn:</Text>
                <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.namemenu}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Số lượng:</Text>
                <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.quantity}</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop:10}}>
                <Text style={{flex: 2, fontSize:16, marginLeft: 10}}>Ghi chú:</Text>
                <Text style={{flex:3, fontSize:16, textAlign:'left'}}>{item.note}</Text>
            </View>

        </View>

    )
}
const mapStateToProps = (state) => ( {
    idhistorydelivery:state.appReducer.idhistorydelivery,
    user:state.appReducer.user
})
const mapDispathToProps = (dispatch)=>({

})
export default connect(mapStateToProps,mapDispathToProps)(DetailHistory);