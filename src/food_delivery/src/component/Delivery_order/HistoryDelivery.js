import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, Dimensions, ScrollView} from 'react-native'
import axios from "axios";
import connect from "react-redux/es/connect/connect";
import ItemHistoryDelivery from "./ItemHistoryDelivery";
import styles from "./Style_HistoryDeli";
import {Actions} from "react-native-router-flux";
const  {height: HEIGHT} = Dimensions.get('window')

class HistoryDelivery extends Component {
    constructor(){
        super()
        this.state={
            datahistory:[],
            erorr:'',
            check: 0,
            showProgress:false,
        }
    }
    componentDidMount(){
        this.handlegetHistoryDelivery()
    }
    handlegetHistoryDelivery(){
        var self=this;
        self.setState({showProgress:true})
        const url='http://food-delivery-server.herokuapp.com/order/getAll';
        const AuthStr = 'Bearer '.concat(this.props.user.token);
        axios.get(url, { headers: { Authorization: AuthStr } })
            .then(res =>{
                self.setState({datahistory:res.data, showProgress:false})
            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1, showProgress:false});
                }
            });

    }
    render() {
        return (
            <View style={{height:HEIGHT,
                width: '100%', backgroundColor:'#E8E8E8'}}>
                <View style={{height: 50}}>
                    <View style={styles.HeaderGioHang}>
                        <TouchableOpacity style={styles.waperButtonclose} onPress={()=>Actions.pop()}>
                            <Image source={require('../img/Back.png')}/>
                        </TouchableOpacity>
                        <Text style={styles.TextGioHang}>Lịch sử mua hàng</Text>
                    </View>
                </View>
                <ScrollView style={{height:HEIGHT-50}}>
                    {this.state.datahistory.map((item)=><ItemHistoryDelivery item={item}/>)}
                </ScrollView>

            </View>
        );
    }
}
const mapStateToProps = (state) => ( {
    user:state.appReducer.user
})
const mapDispathToProps = (dispatch)=>({

})
export default connect(mapStateToProps,mapDispathToProps)(HistoryDelivery);