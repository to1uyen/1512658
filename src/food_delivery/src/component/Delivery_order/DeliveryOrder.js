import React,{Component} from 'react';
import {Image, View, Text, TouchableOpacity, Dimensions} from 'react-native';
import styles from './Style_DeliveryOrder';
import {Actions} from "react-native-router-flux";
import connect from "react-redux/es/connect/connect";
const  {height: HEIGHT} = Dimensions.get('window')
class DeliveryOrder extends Component{
    constructor(props){
        super(props)
        this.state={
            listorder:[]

        }
    }
    componentDidMount(){
       //   var object={idres:0, data:[]}
       //  // alert(JSON.stringify(this.props.data))
       // this.props.data.map((item)=>{
       //     var ch=false;
       //     if(this.state.listorder.length==0){
       //         object = {idres:item.idRestaurant, data:[...data,item]}
       //         this.state.listorder.push(object)
       //     }else{
       //         this.state.listorder.map(e=>{
       //             if(e.idres==item.idRestaurant){
       //                 alert('co roi')
       //                 //e.data.push(item)
       //             }
       //         })
       //         object = {idres:item.idRestaurant, data:[...data,item]}
       //         this.state.listorder.push(object)
       //     }
       //     alert(JSON.stringify(this.state.listorder))
       // })
    }
    render(){
        return(
          <View style={{height:HEIGHT-50,
              width: '100%', backgroundColor:'#E8E8E8'}}>
              <View style={styles.goBack}>
                  <TouchableOpacity style={styles.waperButtonBack} onPress={()=>Actions.pop()}>
                      <Image source={require('../img/Back.png')}/>
                  </TouchableOpacity>
                  <Text style={styles.TextTitle}>Tra sua Gong cha</Text>

              </View>
              <View style={styles.wapermap}>

              </View>
              <View style={styles.waperDirection}>
                  <View style={{flexDirection:'row', alignItems: 'center', flex: 1}}>
                      <Image source={require('../img/circle.png')} style={{width:20, height:20, marginLeft:20}}/>
                      <Text style={{marginLeft: 20, fontSize:17}}>Trà sữa Gong cha{"\n"}123, An Dương Vương, P2, Q5</Text>
                  </View>
                  <View style={{flex:0.5}}>
                      <Image source={require('../img/down.png')} style={{width:40, height:40, marginLeft:10}}/>
                  </View>
                  <View style={{flexDirection:'row', alignItems: 'center', flex:1}}>
                      <Image source={require('../img/circle.png')} style={{width:20, height:20, marginLeft:20}}/>
                      <Text style={{marginLeft: 20, fontSize:17}}>Tố Uyên{"\n"}231/78A, Dương Bá Trạc, P1, Q8</Text>
                  </View>
              </View>
              <View style={styles.waperDetailMon}>
                  <View style={{flexDirection:'row', flex:1,marginLeft:10}}>
                      <Text style={{flex:4, fontSize:17}}>Tổng món:</Text>
                      <Text style={{flex:1, textAlign:'right', right:10 ,fontSize:17}}>3</Text>
                  </View>
                  <View style={{flexDirection:'row', flex:1, marginLeft:10}}>
                      <Text style={{flex:4, fontSize:17}}>Giá:</Text>
                      <Text style={{flex:1, textAlign:'right', right:10 ,fontSize:17}}>25,000đ</Text>
                  </View>
                  <View style={{flexDirection:'row', flex:1, marginLeft:10}}>
                      <Text style={{flex:4, fontSize:17}}>Phí dịch vụ:</Text>
                      <Text style={{flex:1, textAlign:'right', right:10 ,fontSize:17}}>10,000</Text>
                  </View>
                  <View style={{flexDirection:'row', flex:1, marginLeft:10}}>
                      <Text style={{flex:4, fontSize:17}}>Số cây:</Text>
                      <Text style={{flex:1, textAlign:'right', right:10 ,fontSize:17}}>1,5 km</Text>
                  </View>
                  <View style={{flexDirection:'row', flex:1, marginLeft:10}}>
                      <Text style={{flex:4, fontSize:17}}>Phí vận chuyển:</Text>
                      <Text style={{flex:1, textAlign:'right', right:10 ,fontSize:17}}>30,000đ</Text>
                  </View>

              </View>
              <View style={styles.waperDatHang}>
                  <TouchableOpacity style={styles.waperButtonDatHang}>
                      <Text style={styles.TextDatHang}>ĐẶT HÀNG</Text>
                  </TouchableOpacity>
              </View>
          </View>
        );
    }

}
const mapStateToProps = (state) => ({
    data: state.appReducer.data,

})
const mapDispatchToProps = (dispatch) => ({

})
export default connect( mapStateToProps, mapDispatchToProps )(DeliveryOrder)