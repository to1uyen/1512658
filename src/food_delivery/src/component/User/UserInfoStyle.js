import {
    StyleSheet
} from 'react-native';
const  styles=StyleSheet.create({
    logo: {
        width: 180,
        height: 180,
        borderRadius: 100
    },
    text:{
      fontSize:17,
      color:'black',
        flex: 3
    },
    waperText:{
        height: 70,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 5

    },
    buttonchange:{
        backgroundColor:'#99D1D3',
        height:40,
        margin:10,
        borderRadius:5,
        alignItems: 'center',
    }
});
export default styles;