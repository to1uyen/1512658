import React,{Component} from 'react';
import {Image, View, Text, Dimensions, TextInput, TouchableOpacity} from 'react-native';
import styles from './UserInfoStyle';
import {Actions} from "react-native-router-flux";
import {Dropdown} from 'react-native-material-dropdown'
import ImagePicker from 'react-native-image-picker';
const  {height: HEIGHT} = Dimensions.get('window')
import axios from 'axios'
import connect from "react-redux/es/connect/connect";
import {saveinfo, saveavatar} from "../../Actions";

class UserInfo extends Component{
    constructor(props){
        super(props);
        this.state={
            userinfo:{},
            check:0,
            showProgress:false,
            erorr:'',
            arrayAddress:[],
            dataDistrict:[],
            dataWard:[],
            phone:null,
            idDistrict:null,
            idWard:null,
            street:null,
            userName:null,
            urlImage:null,
            nameWard:'',
            nameDistrict:''
        }

    }
    componentDidMount(){
        this.handlegetUserInfo();
        this._getAllDistrict();

    }

    handlegetUserInfo= ()=>{
        var self=this;
        const url='http://food-delivery-server.herokuapp.com/getinfo';
        const AuthStr = 'Bearer '.concat(this.props.user.token);
        axios.get(url, { headers: { Authorization: AuthStr } })
            .then(response => {
                self.setState({userinfo:response.data, phone:response.data.phone, userName:response.data.userName
                ,street:response.data.address.street,idDistrict:response.data.address.idDistrict,idWard: response.data.address.idWard  })
                this.getNameWardAndDistrict();
                 this.props.saveinfo(response.data)
               // this._cutAddress();
            })
            .catch((error) => {
                self.setState({erorr:error})
            });
    }

    getNameWardAndDistrict=()=>{
        this._getAllDistrict()
        let index = this.state.dataDistrict.findIndex(el => el.id == this.state.idDistrict);
        const url = 'http://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id='+this.state.idDistrict;
        //alert(url)
        axios.get(url)
            .then(res =>{
                if(res.data){
                    var newItems = res.data.map(item => ({
                        id: item.id,
                        value: item.name,
                        idDistrict:item.idDistrict
                    }));
                     let index2 = newItems.findIndex(el => el.id == this.state.idWard);
                     this.setState({nameWard:newItems[index].value})
                }else{
                    alert('khong co du lieu')
                }

            })
            .catch((error) => {
                // alert(error.toString())
                this.setState({erorr:error})
            });
         this.setState({nameDistrict:this.state.dataDistrict[index].value})
    }
    _getAllDistrict = ()=>{
        var self=this;
        const url='http://food-delivery-server.herokuapp.com/district/getAll';
        axios.get(url)
            .then(res =>{
                var newItems = res.data.map(item => ({
                    id: item.id,
                    value: item.name
                }));

                self.setState({dataDistrict:newItems})
            })
    }

    _cutAddress =()=>{
        if(this.state.userinfo.address.address){
            // alert(this.state.userinfo.address)
            var myArray = this.state.userinfo.address.split(',');
            this.setState({arrayAddress:myArray, street:myArray[0]})
        }else{
            // alert('lỗi')
        }

    }
    getWardbyDistrict = (index)=>{
        let markers = [...this.state.dataDistrict];
        let pos = markers.findIndex(el => el.value == index);
        var self = this;
        self.setState({idDistrict:markers[pos].id})
        const url = 'http://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id='+markers[pos].id;
        axios.get(url)
            .then(res =>{
                if(res.data){
                    var newItems = res.data.map(item => ({
                        id: item.id,
                        value: item.name,
                        idDistrict:item.idDistrict
                    }));
                    self.setState({dataWard:newItems})
                }else{
                    alert('khong co du lieu')
                }

            })
            .catch((error) => {
                // alert(error.toString())
                self.setState({erorr:error})
            });

    }
    getValueWard = (index)=>{
        let markers = [...this.state.dataWard];
        let pos = markers.findIndex(el => el.value == index);
        var self = this;
        self.setState({idWard:markers[pos].id})
    }

    updateInfoUserNew=()=>{
        var self = this;
        const url = 'http://food-delivery-server.herokuapp.com/updateInfo'
        const AuthStr = 'Bearer '.concat(this.props.user.token);
        this.props.saveavatar(this.state.urlImage)
        var param ={
            phone:this.state.phone,
            idDistrict:this.state.idDistrict,
            idWard:this.state.idWard,
            street:this.state.street,
            userName:this.state.userName
        }
        //alert(JSON.stringify(param))
        axios.post(url, param, {headers:{Authorization:AuthStr}})
            .then(res =>{
                alert('thanh cong')
                this.handlegetUserInfo();
                // alert(res.data)
            }).catch(e => {
                // alert(e.response.data.msg)
            })
    }

    handleUpdateAvatar=()=>{
        const options = {
            title: 'Select Avatar',
        };

        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    urlImage: response.uri
                });
            }
        });
    }
    render(){
        return(
            <View style={{height:HEIGHT,
                width: '100%', backgroundColor:'white'}}>
                <View style={{flexDirection: 'row',
                     flex:1, justifyContent:'center',alignItems:'center', backgroundColor:'#DB7F67'}}>
                    <TouchableOpacity style={{flex:1}} onPress={()=>Actions.Setting()}>
                        <Image source={require('../img/Back.png')} style={{marginLeft:10}}></Image>
                    </TouchableOpacity>
                    <Text style={[styles.text, {flex:9, alignItems:'center', textAlign:'center', color:'white'}]}>THÔNG TIN CÁ NHÂN</Text>
                </View>
                <View style={{flex:9}}>
                    <View style={styles.waperText}>
                        <TouchableOpacity onPress={this.handleUpdateAvatar}>
                            {this.props.urlimage ?<Image source={{uri:this.props.urlimage}} style={{width:70, height:70, borderRadius:100}}/>
                            : <Image source={require('../img/circle.png')} style={{width:70, height:70, borderRadius:100}}/>}
                        </TouchableOpacity>
                        <Text style={[styles.text, { flex:1}]}>Tên:</Text>
                        <TextInput style={[styles.text, { flex:2}]} onChangeText={(userName) => this.setState({userName})} value={this.state.userName}></TextInput>
                    </View>
                    <View style={{backgroundColor:'#ECECEC', height:2}}></View>
                    <View style={styles.waperText}>
                        <Text style={[styles.text, { flex:1}]}>Email:</Text>
                        <Text style={[styles.text, { flex:4}]}>{this.state.userinfo.email}</Text>
                    </View>
                    <View style={{backgroundColor:'#ECECEC', height:2}}></View>
                    <View style={styles.waperText}>
                        <Text style={[styles.text, { flex:2}]}>Số điện thoại:</Text>
                        <TextInput style={[styles.text, { flex:4}]} onChangeText={(phone) => this.setState({phone})} value={this.state.phone} ></TextInput>
                    </View>
                    <View style={{backgroundColor:'#ECECEC', height:2}}></View>
                    <View style={{backgroundColor:'#ECECEC', height:2}}></View>
                    <View>
                        <View style={styles.waperText}>
                            <Text style={[styles.text, {flex:1}]}>Đường: </Text>
                            <TextInput style={[styles.text, { flex:4}]}onChangeText={(street) => this.setState({street})}  value={this.state.street} ></TextInput>
                        </View>
                        <View  style ={{marginLeft: 10}} >
                            <Dropdown  label='Quận'
                                       data={this.state.dataDistrict}
                                       onChangeText={(index) => this.getWardbyDistrict(index)}
                                       value={this.state.nameDistrict}
                            />
                        </View>
                        <View style ={{marginLeft: 10}}>
                            <Dropdown
                                label= 'Phường'
                                data={this.state.dataWard}
                                onChangeText={(index)=>this.getValueWard(index)}
                                value={this.state.nameWard}
                            />
                        </View>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.buttonchange} onPress={this.updateInfoUserNew}>
                            <Text style={styles.text} >LƯU</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </View>
        );
    }

}
const mapStateToProps = (state) => ( {
    user:state.appReducer.user,
    urlimage:state.appReducer.urlimage
})
const mapDispathToProps = (dispatch)=>({
    saveinfo:(object) =>{dispatch(saveinfo(object))},
    saveavatar:(urlimage)=>{dispatch(saveavatar(urlimage))}
})
export default connect(mapStateToProps,mapDispathToProps)(UserInfo)