import React,{Component} from 'react';
import {Image, View, Text, TouchableOpacity, FlatList, TextInput, Dimensions, AsyncStorage} from 'react-native';
import styles from './BasketStyle';
import {Actions} from 'react-native-router-flux'
import {storagefooditem, increasequantity,decreasequantity,deletefooditem, saveprice, savequantity, updatenote, deletebasket} from "../../Actions";
import connect from "react-redux/es/connect/connect";
import axios from 'axios'
const  {height: HEIGHT} = Dimensions.get('window')
class Basket extends Component{
    constructor(props){
        super(props);
        this.state ={
            data:[],
            number:1,
            address:'',
            idres:''
        }

    };
    componentDidMount(){
        this.handlegetMenuRestaurant()
    }




    handlegetdatabasket = async ()=> {
        try {
            const myArray = await AsyncStorage.getItem('itemFood');
                if (myArray !== null) {
                    const parse = JSON.parse(myArray);
                    this.setState({data:[...this.state.data, parse]})
                     const newdata = this.state.data.map((data) => {
                        return {...data, number: 1};
                     });
                    this.setState({data:newdata})
                 }
        } catch (error) {
            alert(error.toString())
        }
    }

    sumQuantity(){
        let tong = 0
        this.props.data.map((item) => {
            tong = tong + item.number;
        });
        const {savequantity} = this.props
        {savequantity(tong)}
        return tong;
    }

    totalPrice(){
        let tong=0;
        this.props.data.map((item)=>{
            let temp = item.number * parseInt(item.price);
            tong = tong + temp;
        })
        const {saveprice} = this.props
        {saveprice(tong)}
        return tong;
    }
    handleupdatenote(e){
        alert(e)

    }
    handlegetMenuRestaurant = async ()=>{
        var self=this;
        var idRestautant=await AsyncStorage.getItem('idRestaurant')
        const url='http://food-delivery-server.herokuapp.com/restaurant/getMenu/'+ idRestautant;
        axios.get(url)
            .then(res =>{
                self.setState({address:res.data.address.address, idres:idRestautant})
            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1});
                }
            });
    }

    handleCreateOder=()=>{
        const url = 'http://food-delivery-server.herokuapp.com/order/create'
        const AuthStr = 'Bearer '.concat(this.props.user.token);
        let newItems = this.props.data.map(item => ({
            idFood: item.id,
            quantity: item.number,
            note:''
        }));
        const order = {
            totalPrice: this.props.price,
            Street:this.props.info.address.street,
            idWard:this.props.info.address.idWard,
            idDistrict:this.props.info.address.idDistrict,
            phone:this.props.info.phone,
            idRestaurant:this.state.idres,
            item:newItems

        };
        //alert(JSON.stringify(order))
        axios.post(url, order,{ headers: { Authorization: AuthStr }})
            .then(response => {
                this.props.deletebasket()
                Actions.Deliveryoder()
            })

        //Actions.Deliveryoder
        //axios.post(url, )
    }

    render(){
        return(
            <View style={{height:HEIGHT-50,
                width: '100%', backgroundColor:'#E8E8E8'}}>
                <View style={styles.HeaderGioHang}>
                    <TouchableOpacity style={styles.waperButtonclose} onPress={()=>Actions.pop()}>
                        <Image source={require('../img/close.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.TextGioHang}>Giỏ hàng</Text>
                </View>
                <View style={styles.waperGioHang}>
                    <View style={{flexDirection: 'row', marginTop:5}}>
                        <Text style={styles.title}>Tổng số lượng: </Text>
                        <Text style={styles.value}>{this.sumQuantity()}</Text>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Text style={styles.title}>Tổng giá tiền: </Text>
                        <Text style={styles.value}>{this.totalPrice()} VNĐ</Text>
                    </View>
                </View>
                <View style={styles.waperChiTietGioHang}>
                    <FlatList
                        data={this.props.data}
                        renderItem={this._renderItem}
                        keyExtractor = { (item,index) => index.toString()}
                    />

                </View>
                <View style={styles.waperGiaoHang}>
                    <TouchableOpacity style={styles.waperButtonGiaoHang} onPress={()=>this.handleCreateOder()}>
                        <Text style={styles.TextGiaoHang}>GIAO HÀNG</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    _renderItem=({item})=>(
        <View style={{ marginBottom:10, backgroundColor: 'white'}}>
           <View style={{flexDirection:'row'}}>
               <Image source={{uri:item.image}} style={{flex:3}}></Image>
               <View style={{flex:7, marginLeft:5}}>
                   <Text style={[{flex:1}, styles.value]}>{item.name}</Text>
                   <Text style={[{flex:1}, styles.title]}>{item.number} x {item.price} = {item.number * item.price}</Text>
                   <View style={{flexDirection:'row',flex:1, marginBottom: 10, alignItems: 'center'}}>
                       <TouchableOpacity onPress={()=>{this.props.decreasequantity(item.id)}}>
                           <Image source={require('../img/minus.png')} style={{width:30, height:30}}></Image>
                       </TouchableOpacity>
                       <TextInput style={styles.Textinput}>{item.number}</TextInput>
                       <TouchableOpacity onPress={()=>{this.props.increasequantity(item.id)}}>
                           <Image source={require('../img/plus.png')} style={{width:30, height:30}}></Image>
                       </TouchableOpacity>
                       <TouchableOpacity onPress = {()=>{this.props.deletefooditem(item.id)}} style={{marginLeft:20}}>
                           <Image source={require('../img/delete.png')} style={{ width:30, height:30, marginRight:10}}></Image>
                       </TouchableOpacity>
                   </View>
               </View>
           </View>

        </View>

    )
}

const mapStateToProps = (state) => ( {
    data: state.appReducer.data,
    price:state.appReducer.price,
    quantity:state.appReducer.quantity,
    info:state.appReducer.info,
    user:state.appReducer.user
})

const mapDispatchToProps = (dispatch) => ({
    storagefooditem: (item) => { dispatch(storagefooditem(item)) },
    increasequantity:(id) =>{dispatch(increasequantity(id))},
    decreasequantity:(id) =>{dispatch(decreasequantity(id))},
    deletefooditem:(id) =>{dispatch(deletefooditem(id))},
    saveprice:(price) => {dispatch(saveprice(price))},
    savequantity:(quantity) => {dispatch(savequantity(quantity))},
    updatenote:(id)=>{dispatch(updatenote(id))},
    deletebasket:()=>{dispatch(deletebasket())}
})

export default connect( mapStateToProps, mapDispatchToProps )(Basket)
