import {
    StyleSheet
} from 'react-native';
const  styles=StyleSheet.create({
    HeaderGioHang:{
        flexDirection:'row',
        flex: 1,
        backgroundColor:'#DB7F67',
        alignItems:'center',
    },
    waperButtonclose:{
        flex:1
    },
    waperGioHang:{
        flex: 1,
        backgroundColor: '#FFFBD1'
    },
    waperChiTietGioHang:{
        flex:7
    },
    waperGiaoHang:{
        flex:0.7
    },
    TextGioHang:{
        flex: 7,
        textAlign: 'center',
        fontSize:20,
        fontWeight: 'bold',
        color:'white'
    },
    title:{
        fontSize: 17,
        color: 'black'
    },
    value:{
        fontSize:17,
        fontWeight: 'bold',
        color:'black'
    },
    Textinput:{
        fontSize: 17,
        textAlign: 'center',
        backgroundColor:'white'
    },
    waperButtonGiaoHang:{
        backgroundColor:'#DB7F67',
        justifyContent: 'center',
        alignItems: 'center',
        flex:1
    }
});
export default styles;