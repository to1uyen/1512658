import {
    StyleSheet
} from 'react-native';
const  styles=StyleSheet.create({
    LoginContainer:{

        justifyContent: 'center',
        alignItems: 'center',
        // resizeMode: 'stretch'
    },
    LogoContainer:{
        flex:3,
        marginTop:10
    },
    logo:{
        width: 180,
        height:180,
        borderRadius:100
    },
    waperTextInput:{
       flex:7,
        marginTop: 10,
        width:'100%',
        alignItems: 'center',
    },
    TextInput:{
        borderWidth: 1,
        height:40,
        width:'80%',
        borderRadius:5,
        marginTop: 5,
        fontSize:15,

    },
    waperButtonLogin:{
        backgroundColor:'#DB7F67',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:40,
        width:'80%',
        height:40,
        borderRadius:5
    },
    TextLogin:{
        fontWeight: 'bold',
        fontSize:15,
        width:'100%',
        textAlign: 'center'
    },
    RowInLogin:{
        flexDirection: 'row',
        alignItems:'center',
        marginTop:10,

    },
    Separator:{
        height:2,
        backgroundColor: '#555555',
        flex:1
    },
    waperButtonLoginFacebook:{
        backgroundColor:'#184785',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:10,
        width:330,
        height:40
    },
    TextLoginFacebook:{
        fontWeight: 'bold',
        fontSize:15,
        color:'#FFFFFF'
    },
    RowRegister:{
        flexDirection: 'row',
        marginTop:20
    },
    textforgotpass:{
        fontSize:17,
        textDecorationLine:'underline',
        marginTop:10,
        width:330,
        textAlign: 'right'
    },
    error:{
        color: 'red',
        fontSize:17,
        marginTop:5
    }

});
export default styles;