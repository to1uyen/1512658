import React,{Component} from 'react';
import {
    Image,
    StatusBar,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    Alert,
    ActivityIndicator
} from 'react-native';
import styles from '../Styles/style';
import {Actions} from "react-native-router-flux";
import axios from "axios";
const  {height: HEIGHT} = Dimensions.get('window')

export default class ForgotPassword extends Component {
    constructor(props){
        super(props);
        this.state={
            email:'',
            erorr:'',
            check:0,
            showProgress: false
        }
    }
    handleforgetPassword(){
        var self=this;
        self.setState({showProgress:true});
        const url='http://food-delivery-server.herokuapp.com/forgetPassword';
        const user = {
            email: this.state.email,
        };
        axios.post(url,user)
            .then(res =>{
                self.setState({check:1, showProgress:false});
                Alert.alert(
                    'Thông báo',
                    res.data.msg,
                    [
                        {text: 'OK', onPress: () => Actions.login()},
                    ],
                    { cancelable: false }
                )

            })
            .catch(function (error) {
                if (error.response) {
                    self.setState({erorr:error.response.data.msg, check:-1, showProgress:false});
                }
            });
    }
    render() {
        return(
            <ImageBackground source={require('../img/background.jpg')}style={{flex:1}}>
                <StatusBar hidden={true}/>
            <View style={[styles.LoginContainer,{ height:HEIGHT,
                width: '100%'}]}>
                <View style={{flexDirection: 'row',
                    alignItems: 'center', flex:3}}>
                    <TouchableOpacity style={{flex:0.5}} onPress={()=>Actions.login()}>
                        <Image source={require('../img/Back.png')} style={{marginLeft:10}}></Image>
                    </TouchableOpacity>
                    <View style={{justifyContent: 'center',
                        alignItems: 'center', flex:9.5}} >
                        <Image source={require('../img/logo.jpg')} style={styles.logo}/>
                    </View>
                </View>
                <View style={styles.waperTextInput}>
                    <TextInput placeholder={'Email'} style={styles.TextInput} onChangeText={(email)=> this.setState({email})}/>
                    { this.state.showProgress &&
                    <ActivityIndicator animating={true} size="large" color="#008489"/>
                    }
                    {this.state.check==-1 && <Text style={styles.error}>{this.state.erorr}</Text>}
                    <TouchableOpacity style={styles.waperButtonLogin} onPress={()=>this.handleforgetPassword()}>
                        <Text style={styles.TextLogin}>LẤY LẠI MẬT KHẨU</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </ImageBackground>
        );
    }
}