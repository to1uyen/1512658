import React,{Component} from 'react';
import {Image, StatusBar, View, Text, TextInput, TouchableOpacity, ImageBackground,Dimensions,Alert, ActivityIndicator, AsyncStorage} from 'react-native';
import styles from '../Styles/style';
import {Actions} from 'react-native-router-flux'
import {authenticate}from '../.././Actions'
import connect from "react-redux/es/connect/connect";

const  {height: HEIGHT} = Dimensions.get('window')

class LoginFood_Delivery extends Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            password:'',
            error:''
        }

    }
    componentDidMount()
    {
        const { isAuthenticated } = this.props;
        if (isAuthenticated) {
            setTimeout(() => {
                Actions.tabbar();
            }, 10);
        }
    }

    componentWillReceiveProps(props)
    {
        const { isAuthenticated, error } = props;
        if(isAuthenticated)
        {
            Actions.tabbar();
        }
        if(error){
            this.setState({error:error})
        }
    }

    handleSignIn(){
        const { authenticateF } = this.props;

        authenticateF({
            email:this.state.email,
            password: this.state.password
        })
    }

    render(){
        return(
            <ImageBackground source={require('../img/background.jpg')}style={{flex:1}}>
                <StatusBar hidden={true}/>
                <View style={[styles.LoginContainer,{ height:HEIGHT,
                    width: '100%'}]}>
                    <View style={styles.LogoContainer} >
                        <Image source={require('../img/logo.jpg')} style={styles.logo}/>
                    </View>
                    <View style={styles.waperTextInput}>
                        <TextInput placeholder={'Tên Đăng Nhập'} style={styles.TextInput} autoCapitalize='none' onChangeText={(email) => this.setState({email})}/>
                        <TextInput placeholder={'Mật khẩu'} style={styles.TextInput} secureTextEntry={true} autoCapitalize='none' onChangeText={(password) => this.setState({password})}/>
                        { this.props.isAuthenticating &&
                        <ActivityIndicator animating={true} size="large" color="#008489"/>
                        }
                        {this.state.error!=='' && <Text style={styles.error}>{this.props.error}</Text>}
                        <TouchableOpacity style={styles.waperButtonLogin} onPress={()=>this.handleSignIn()} disabled={this.props.isAuthenticating}>
                            <Text style={styles.TextLogin}>ĐĂNG NHẬP</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.textforgotpass} onPress={()=>Actions.fogotpassword()}>Quên mật khẩu ?</Text>
                        </TouchableOpacity>
                        <View style={styles.RowRegister}>
                            <Text style={{fontSize:17}}>Bạn chưa có tài khoản?</Text>
                            <TouchableOpacity>
                                <Text style={{textDecorationLine:'underline', fontSize:17}} onPress={()=>Actions.register()}> TẠO TÀI KHOẢN</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground>

        );
    }
}

const mapDispatchToProps = (dispatch)=> ({
    authenticateF: (user) => {
        dispatch(authenticate(user));
    }
})
const mapStateToProps = (state)=>({
    isAuthenticated: state.appReducer.isAuthenticated,
    isAuthenticating:state.appReducer.isAuthenticating,
    user: state.appReducer.user,
    error:state.appReducer.error
})
export default connect( mapStateToProps,mapDispatchToProps)(LoginFood_Delivery)