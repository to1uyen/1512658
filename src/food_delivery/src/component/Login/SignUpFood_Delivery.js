import React,{Component} from 'react';
import {
    Image,
    StatusBar,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    Alert,
    ActivityIndicator
} from 'react-native';
import styles from '../Styles/style';
import axios from 'axios';
import {Actions} from'react-native-router-flux'
import connect from "react-redux/es/connect/connect";
import {registeruser} from "../../Actions/login";

const  {height: HEIGHT} = Dimensions.get('window')

class SignUpFood_Delivery extends Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            password:'',
            repeatpassword:'',
            check:0,
            erorr:'',
            showProgress: false
        }
    }


    handleSignUp(){

        this.setState({showProgress:true})
        const obj = {
            password:this.state.password,
            repeatpassword:this.state.repeatpassword,
            email:this.state.email
        }
        this.props.registeruser(obj)
        const {point, error} =this.props;
        alert(point)
        if(point==true){
            this.setState({check:1, showProgress:false});
            {Actions.login()};
        }else{
            this.setState({erorr:error, check:-1, showProgress:false});
        }

    }

    render(){
        return(
            <ImageBackground source={require('../img/background.jpg')}style={{flex:1}}>
                <StatusBar hidden={true}/>
            <View style={[styles.LoginContainer,{ height:HEIGHT,
                width: '100%'}]}>
                <View style={{flexDirection: 'row',
                    alignItems: 'center',flex:3}}>
                    <TouchableOpacity style={{flex:0.5}} onPress={()=>Actions.login()}>
                        <Image source={require('../img/Back.png')} style={{marginLeft:10}}></Image>
                    </TouchableOpacity>
                    <View style={{justifyContent: 'center',
                        alignItems: 'center', flex:9.5}} >
                        <Image source={require('../img/logo.jpg')} style={styles.logo}/>
                    </View>
                </View>

                <View style={styles.waperTextInput}>
                    <TextInput placeholder={'Email'} style={styles.TextInput} autoCapitalize='none' onChangeText={(email) => this.setState({email})}/>
                    <TextInput placeholder={'Mật khẩu'} style={styles.TextInput} autoCapitalize='none' secureTextEntry={true} onChangeText={(password) => this.setState({password})}/>
                    <TextInput placeholder={'Nhập lại mật khẩu'} style={styles.TextInput} autoCapitalize='none' secureTextEntry={true} onChangeText={(repeatpassword) => this.setState({repeatpassword})}/>
                    { this.state.showProgress &&
                    <ActivityIndicator animating={true} size="large" color="#008489"/>
                    }
                    {this.state.check == -1  && <Text style={styles.error}>{this.state.erorr}</Text>}
                    <TouchableOpacity style={styles.waperButtonLogin}>
                        <Text style={styles.TextLogin} onPress={()=>this.handleSignUp()}>ĐĂNG KÝ</Text>
                    </TouchableOpacity>

                </View>
            </View>
            </ImageBackground>
        );
    }

}

const mapStateToProps=(state)=>({
    error:state.appReducer.error,
    point:state.appReducer.point
})

const mapDispathToProps = (dispatch)=>({
    registeruser:(obj)=>{
        dispatch(registeruser(obj))
    }
})
export default connect(mapStateToProps,mapDispathToProps)(SignUpFood_Delivery)