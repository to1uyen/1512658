import React,{Component} from 'react';
import {
    Image,
    StatusBar,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
    Alert,
    ActivityIndicator
} from 'react-native';
import styles from '../Styles/style';
import axios from 'axios';
import {Actions} from'react-native-router-flux'
import {signOut} from "../../Actions";
import {updatepassword} from "../../Actions/login"
import connect from "react-redux/es/connect/connect";
import I18n from '../../I18n/I18n'

const  {height: HEIGHT} = Dimensions.get('window')

class UpdatePassword extends Component{
    constructor(props){
        super(props);
        this.state={
            password:'',
            repeatpassword:'',
            newpassword:'',
            check:0,
            erorr:'',
            showProgress: false
        }
    }
    componentWillReceiveProps(props)
    {
        const {point, error} =props;
        if(point==true){
            this.setState({check:1, showProgress:false});
            this.props.signOut();
            {Actions.login()};
        }else{
            this.setState({erorr:error, check:-1, showProgress:false});
        }
    }

    handleUpdate(){
        this.setState({showProgress:true})
        const AuthStr = 'Bearer '.concat(this.props.user.token);
        this.props.updatepassword({newpassword:this.state.newpassword,repeatpassword:this.state.repeatpassword,
        password:this.state.password,AuthStr:AuthStr })
        const {point, error} =this.props;
        if(point==true){
            this.setState({check:1, showProgress:false});
            this.props.signOut();
            {Actions.login()};
        }else{
            this.setState({erorr:error, check:-1, showProgress:false});
        }
    }

    render(){
        return(
            <ImageBackground source={require('../img/background.jpg')}style={{flex:1}}>
                <StatusBar hidden={true}/>
                <View style={[styles.LoginContainer,{ height:HEIGHT,
                    width: '100%'}]}>
                    <View style={{flexDirection: 'row',
                        alignItems: 'center',flex:3}}>
                        <TouchableOpacity style={{flex:0.5}} onPress={()=>Actions.pop()}>
                            <Image source={require('../img/Back.png')} style={{marginLeft:10}}></Image>
                        </TouchableOpacity>
                        <View style={{justifyContent: 'center',
                            alignItems: 'center', flex:9.5}} >
                            <Image source={require('../img/logo.jpg')} style={styles.logo}/>
                        </View>
                    </View>

                    <View style={styles.waperTextInput}>
                        <TextInput placeholder={'Mật khẩu hiện tại'} style={styles.TextInput} autoCapitalize='none' secureTextEntry={true} onChangeText={(password) => this.setState({password})}/>
                        <TextInput placeholder={'Mật khẩu mới'} style={styles.TextInput} autoCapitalize='none' secureTextEntry={true} onChangeText={(newpassword) => this.setState({newpassword})}/>
                        <TextInput placeholder={'Nhập lại mật khẩu mới'} style={styles.TextInput} autoCapitalize='none' secureTextEntry={true} onChangeText={(repeatpassword) => this.setState({repeatpassword})}/>
                        { this.state.showProgress &&
                        <ActivityIndicator animating={true} size="large" color="#DB7F67"/>
                        }
                        {this.state.check == -1  && <Text style={styles.error}>{this.state.erorr}</Text>}
                        <TouchableOpacity style={styles.waperButtonLogin}>
                            <Text style={styles.TextLogin} onPress={()=>this.handleUpdate()}>{I18n.t('confirm')}</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </ImageBackground>
        );
    }

}
const mapStateToProps = (state) => ( {
    user:state.appReducer.user,
    error:state.appReducer.error,
    point:state.appReducer.point
})
const mapDispathToProps = (dispatch)=>({
    signOut: () => {
        dispatch(signOut());
    },
    updatepassword:(obj)=>{
        dispatch(updatepassword(obj))
    }

})
export default connect(mapStateToProps,mapDispathToProps)(UpdatePassword)