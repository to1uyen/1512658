import React, {Component} from 'react';
import {Dimensions, View, Text} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import Styles from './styleSetting'
import {Actions} from "react-native-router-flux";
import {signOut, savelanguage, saveindexlanguage} from "../../Actions";
import connect from "react-redux/es/connect/connect";
import SwitchSelector from 'react-native-switch-selector';
import I18n from '../../I18n/I18n'
import RNRestart from 'react-native-restart';
const  {height: HEIGHT} = Dimensions.get('window')
const {width:WIDTH} = Dimensions.get('window').width;

class Setting extends Component {
    constructor(props){
        super(props);
        this.state ={
            options:[
                    { label: 'Việt Nam', value: 'vn'},
                    { label: 'English', value: 'en'}]
        }
    }
    handleChange(val){
        let index = this.state.options.findIndex(el => el.value == val);
        this.props.saveindexlanguage(index);
        this.props.savelanguage(val)
        setInterval(()=>{
            RNRestart.Restart();
        }, 1000)

    }

    render() {
        return (
            <View style={{ height:HEIGHT-70,
                width: '100%'}}>
                <Icon.Button name="user" size={20} color="#DB7F67" style={Styles.waperItem} onPress={()=>Actions.UserInfo()}>
                    <Text>{I18n.t('Userinfo')}</Text>
                </Icon.Button>
                <Icon.Button name="language" size={20} color="#DB7F67" style={Styles.waperItem}>
                    <Text>{I18n.t('language')}</Text>
                </Icon.Button>
                <SwitchSelector
                    initial={this.props.indexlanguage}
                    onPress={val =>this.handleChange(val)}
                    textColor='#7a44cf' //'#7a44cf'
                    selectedColor='white'
                    buttonColor='#DB7F67'
                    borderColor='#DB7F67'
                    hasPadding
                    options={this.state.options} />
                <Icon.Button name="history" size={20} color="#DB7F67" style={Styles.waperItem} onPress={()=>Actions.HistoryDelivery()}>
                    <Text>{I18n.t('Buyhistory')}</Text>
                </Icon.Button>
                <Icon.Button name="key" size={20} color="#DB7F67" style={Styles.waperItem} onPress={()=>Actions.UpdatePassword()}>
                    <Text>{I18n.t('Changepassword')}</Text>
                </Icon.Button>
                <Icon.Button name="sign-out" size={20} color="#DB7F67" style={Styles.waperItem} onPress={()=>{this.props.signOut();
                    Actions.login()}}>
                    <Text>{I18n.t('Signout')}</Text>
                </Icon.Button>
            </View>
        );
    }
}
const mapStateToProps = (state) => ( {
    indexlanguage:state.appReducer.indexlanguage

})
const mapDispathToProps = (dispatch)=>({
    signOut: () => {
        dispatch(signOut());
    },
    savelanguage:(val)=>{dispatch(savelanguage(val))},
    saveindexlanguage:(index)=>{dispatch(saveindexlanguage(index))}
})
export default connect(mapStateToProps,mapDispathToProps)(Setting);