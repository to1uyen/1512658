import I18n from 'react-native-i18n';
import en from './en';
import vn from './vn';
    I18n.fallbacks = true;
    I18n.translations = {
        vn,
        en
    };
export default I18n