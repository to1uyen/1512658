import axios from 'axios'

export const UPDATEPASSWORD_SUCCESS = 'UPDATEPASSWORD_SUCCESS'
export const UPDATEPASSWORD_FAIL = 'UPDATEPASSWORD_FAIL'

export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'


export const updatepassword=(obj)=>(dispatch)=>{
    if(obj.newpassword !== obj.repeatpassword){
        dispatch({
            type: UPDATEPASSWORD_FAIL,
            data: 'mật khẩu không chính xác',
        })
    }else if(obj.newpassword.trim() == '' || obj.repeatpassword.trim() == '' || obj.password.trim() ==''){
        dispatch({
            type: UPDATEPASSWORD_FAIL,
            data: 'mật khẩu không được để trống',
        })

    }else{
        //alert(JSON.stringify(obj))
        const password={
            password: obj.newpassword
        }
        axios.post('http://food-delivery-server.herokuapp.com/updatePassword', password, {headers:{Authorization:obj.AuthStr}})
            .then (response => {
            dispatch({
                type: UPDATEPASSWORD_SUCCESS,
            })
        }).catch(e => {
            dispatch({
                type: UPDATEPASSWORD_FAIL,
                data: e.response.data.msg,
            })
        })
    }
}

export const registeruser=(obj)=>(dispatch)=>{
    //alert(JSON.stringify(obj))
    if(obj.password !== obj.repeatpassword){
        dispatch({
            type: REGISTER_FAIL,
            data: 'mật khẩu không chính xác',
        })
    }else if(obj.email.trim() == '' || obj.repeatpassword.trim() == '' || obj.password.trim() ==''){
        dispatch({
            type: REGISTER_FAIL,
            data: 'Vui lòng không được để trống',
        })

    }else{

        const user={
            email:obj.email,
            password: obj.newpassword
        }
        axios.post('http://food-delivery-server.herokuapp.com/register', user)
            .then (response => {
                dispatch({
                    type: REGISTER_SUCCESS,
                })
            }).catch(e => {
            dispatch({
                type: REGISTER_FAIL,
                data: e.response.data.msg,
            })
        })
    }
}
