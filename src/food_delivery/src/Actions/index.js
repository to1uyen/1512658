import axios from 'axios'
export const STORAGEFOODITEM = 'STORAGEFOODITEM'
export const DELETEFOODITEM = 'DELETEFOODITEM'
export const INCREASEQUATITY = 'INCREASEQUATITY'
export const DECREASEQUATITY = 'DECREASEQUATITY'
export const SAVEPRICE = 'SAVEPRICE'
export const SAVEQUANTITY = 'SAVEQUANTITY'
export const SAVEUSER = 'SAVEUSER'
export const SAVEINFO = 'SAVEINFO'
export const UPDATNOTE = 'UPDATNOTE'

//autologin

export const AUTHENTICATION_REQUEST = 'AUTHENTICATION_REQUEST';
export const AUTHENTICATION_SUCCESS = 'AUTHENTICATION_SUCCESS';
export const AUTHENTICATION_FAILURE = 'AUTHENTICATION_FAILURE';
export const SIGN_OUT = 'SIGN_OUT';
///
export const SAVELANGUAGE = 'SAVELANGUAGE'
export const SAVEINDEXLANGUAGE = 'SAVEINDEXLANGUAGE'
export const SAVEIDHISTORYDELIVERY = 'SAVEIDHISTORYDELIVERY'

///
export const DELETEBASKET ='DELETEBASKET'
//
export const SAVEAVATAR = 'SAVEAVATAR'

export const saveavatar=(urlimage)=>{
    return{
        type:SAVEAVATAR,
        data:urlimage
    }
}

export const deletebasket =()=>{
    return{
        type:DELETEBASKET
    }
}



export const saveinfo = (object)=>{
    return{
        type:SAVEINFO,
        data:object
    }
}

export const saveidhistorydelivery = (id)=>{
    return{
        type:SAVEIDHISTORYDELIVERY,
        data:id
    }
}
    export const savelanguage = (val)=>{
        return {
                type:SAVELANGUAGE,
                data:val
        }
    }
    export const saveindexlanguage = (indexlanguage)=>{
        return {
            type:SAVEINDEXLANGUAGE,
            data: indexlanguage,
        };
    }


export const storagefooditem = (item) => {
    return {
        type:STORAGEFOODITEM,
        data: item,
    };
}

export const deletefooditem = (id) => {
    return {
        type:DELETEFOODITEM,
        data: id,
    };
}

export const increasequantity = (id) => {
    return {
        type:INCREASEQUATITY,
        data: id,
    };
}

export const updatenote = (id)=>{
    return{
        type:UPDATNOTE,
        data:id
    }
}

export const decreasequantity = (id) =>{
    return{
      type: DECREASEQUATITY,
      data:id,
    };
}

export const saveprice = (price) =>{

    return {
        type:SAVEPRICE,
        data:price
    }
}

export const savequantity = (quantity) =>{
    return{
        type:SAVEQUANTITY,
        data:quantity
    }
}

export const saveuser = (user) =>{
    return{
        type:SAVEUSER,
        data:user
    }
}

export const authenticate = (user) => (dispatch) => {
    dispatch({
        type: AUTHENTICATION_REQUEST,
    })
    if(user.email==='' && user.password===''){
        dispatch({
            type: AUTHENTICATION_FAILURE,
            data: 'Vui lòng nhập email và mật khẩu',
        })
    }else if(user.email===''){
        dispatch({
            type: AUTHENTICATION_FAILURE,
            data: 'Vui lòng nhập email',
        })
    }else if(user.password===''){
        dispatch({
            type: AUTHENTICATION_FAILURE,
            data: 'Vui lòng nhập password',
        })
    }else{
        axios.post('https://food-delivery-server.herokuapp.com/login', user).then (response => {
            dispatch({
                type: AUTHENTICATION_SUCCESS,
                data: response.data,
            })
        }).catch(e => {
            dispatch({
                type: AUTHENTICATION_FAILURE,
                data: e.response.data.msg,
            })
        })
    }

}

export const signOut = () => (dispatch) => {
    dispatch({
        type: SIGN_OUT,
    });
}




