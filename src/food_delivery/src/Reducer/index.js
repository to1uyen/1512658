import {STORAGEFOODITEM, INCREASEQUATITY, DECREASEQUATITY, DELETEFOODITEM, SAVEPRICE, SAVEQUANTITY, SAVEUSER,
    AUTHENTICATION_SUCCESS,
    AUTHENTICATION_FAILURE,
    AUTHENTICATION_REQUEST,
    SIGN_OUT, SAVELANGUAGE, SAVEINDEXLANGUAGE, SAVEIDHISTORYDELIVERY, SAVEINFO, UPDATNOTE, DELETEBASKET, SAVEAVATAR} from '../Actions'
import {UPDATEPASSWORD_FAIL, UPDATEPASSWORD_SUCCESS, REGISTER_SUCCESS,REGISTER_FAIL} from "../Actions/login";

const initialState={
    data:[],
    price:0,
    quantity:0,
    user:{},
    isAuthenticating: false,
    isAuthenticated: false,
    error:'',
    token:'',
    language:'vn',
    indexlanguage:0,
    idhistorydelivery:null,
    point:false,
    info:{},
    listorder:[],
    urlimage:''
}

const appReducer = (prevState = initialState, action) => {
    switch(action.type)
    {
        case SAVEAVATAR:
            return{
                ...prevState, urlimage: action.data
            }
        case SAVEINFO:
            return{
                ...prevState, info:action.data
            }
        case STORAGEFOODITEM:
            var newInput = Object.assign({},
                action.data, {['number']: 1, ['note']: ''});

            return {
                ...prevState,
                data: [...prevState.data, newInput]
            }

        case INCREASEQUATITY:
            let markers
            prevState.data.find((e)=>{
                markers = [...prevState.data];
                let index = markers.findIndex(el => el.id == action.data);
                markers[index] = {...markers[index], number: parseInt(prevState.data[index].number.toString())+1};
            })
            return {
                ...prevState, data:markers
            }
        case DECREASEQUATITY:
            prevState.data.find((e)=>{
                markers = [...prevState.data];
                let index = markers.findIndex(el => el.id == action.data);
                if(parseInt(prevState.data[index].number -1 ) <1){
                    markers[index] = {...markers[index], number: 1};
                }else{
                    markers[index] = {...markers[index], number: parseInt(prevState.data[index].number)-1};

                }
            })
            return {
                ...prevState, data:markers
            }
        case UPDATNOTE:
            prevState.data.find((e)=>{
                markers = [...prevState.data];
                let index = markers.findIndex(el => el.id == action.data.id);
                markers[index] = {...markers[index], note:action.data.note};
            })
            return {
                ...prevState, data:markers
            }
        case SAVEPRICE:
            return{
                ...prevState, price: action.data
            }
        case SAVEQUANTITY:
            return {
                ...prevState, quantity: action.data
            }

        case DELETEFOODITEM:

            prevState.data.find((e)=>{
                markers = [...prevState.data];
                let index = markers.findIndex(el => el.id == action.data);
                markers.splice(index,1)
            })
            return {
                ...prevState, data:markers
            }
        case DELETEBASKET:
            return{...prevState, data:[],price:0, quantity:0}
        case SAVEUSER:
            return{
                ...prevState, user:action.data
            }
        case AUTHENTICATION_SUCCESS:
        {
            return { ...prevState,
                isAuthenticating: false,
                isAuthenticated: true,
                error:'',
                user: action.data,
                token:action.data.token
            };
        }
        case AUTHENTICATION_REQUEST:
        {
            return { ...prevState,
                isAuthenticating: true,
                isAuthenticated: false,
                user: null,
                token:null,
                error:''
            };
        }
        case AUTHENTICATION_FAILURE:
        {
            return { ...prevState,
                isAuthenticated: false,
                isAuthenticating: false,
                user: null,
                token:null,
                error: action.data
            };
        }
        case SIGN_OUT:
        {
            return { ...prevState,
                isAuthenticating:false,
                isAuthenticated:false,
                token:null,
                user:null,
                error:'',
                point:false
            };
        }
        case UPDATEPASSWORD_SUCCESS:{
            return {
                ...prevState,
                point: true,
                error:''
            }
        }
        case UPDATEPASSWORD_FAIL:{
            return{
                ...prevState,
                point:false,
                error:action.data
            }
        }
        case REGISTER_SUCCESS:{
            return{
                ...prevState,
                point:true,
                error:''
            }
        }
        case REGISTER_FAIL:{
            return{
                ...prevState,
                point:false,
                error:action.data
            }
        }
        case SAVELANGUAGE:{
            return{...prevState,
                language: action.data
            };
        }
        case SAVEINDEXLANGUAGE:{
            return{...prevState,
                indexlanguage: action.data
            };
        }
        case SAVEIDHISTORYDELIVERY:{
            return{...prevState,
                idhistorydelivery: action.data
            }
        }
        default:
            return prevState;
    }
}


export default appReducer;