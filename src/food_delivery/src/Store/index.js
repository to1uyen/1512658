import {applyMiddleware, createStore, compose} from "redux";
import thunk from "redux-thunk";
import {persistStore} from "redux-persist";
import rootReducer from './reducer'

export const store = createStore(rootReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);