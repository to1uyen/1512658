import storage from "redux-persist/lib/storage";
import {combineReducers} from "redux";
import {persistReducer} from "redux-persist";
import appReducer from "../Reducer/index";

const authenticationConfig = {
    key: 'authentiation',
    storage,
    blacklist: ['isAuthenticating']
}

const rootReducer = combineReducers({
    appReducer: persistReducer(authenticationConfig, appReducer),
    // ==> merchantReducer: merchantReducer,
})
export default rootReducer;