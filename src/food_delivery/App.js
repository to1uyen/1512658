/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Router, Scene} from 'react-native-router-flux'
import LoginFood_Delivery from './src/component/Login/LoginFood_Delivery';
import SignUpFood_Delivery from "./src/component/Login/SignUpFood_Delivery";
import ForgotPassword from "./src/component/Login/ForgotPassword";
import Merchant from "./src/component/Merchant/Merchant";
import DeliveryOrder from "./src/component/Delivery_order/DeliveryOrder";
import Icon from 'react-native-vector-icons/FontAwesome5'
import UserInfo from "./src/component/User/UserInfo";
import Basket from "./src/component/Basket/Basket";
import MerchantDetail from "./src/component/Merchant/MerchantDetail";


import Map from "./src/component/Map/Map";
import Setting from "./src/component/Setting/Setting";
import UpdatePassword from "./src/component/Login/UpdatePassword";
import I18n from './src/I18n/I18n'
import connect from "react-redux/es/connect/connect";
import HistoryDelivery from "./src/component/Delivery_order/HistoryDelivery";
import DetailHistory from "./src/component/Delivery_order/DetailHistory";

type Props = {};

const myIconHome =() => (<Icon name="home" size={20} color="#DB7F67" />)
const myIconBasket =() => (<Icon name="shopping-basket" size={20} color="#DB7F67" />)
const myIcondeliveryoder =() => (<Icon name="motorcycle" size={20} color="#DB7F67" />)
const myIconSetting=() =>(<Icon name="cog" size={20} color="#DB7F67"/>)
const myIconMap=()=>(<Icon name="map-marked-alt" size={20} color="#DB7F67"/>)

class App extends Component<Props> {
    componentDidMount(){
        I18n.locale = this.props.language

    }
  render() {
    return (
        <Router>
            <Scene key="root">
                <Scene
                    key="login"
                    component={LoginFood_Delivery}
                    inittial
                    hideNavBar={true}
                />
                <Scene
                    key="register"
                    component={SignUpFood_Delivery}
                    hideNavBar={true}
                />
                <Scene
                    key="fogotpassword"
                    component={ForgotPassword}
                    hideNavBar={true}
                />
                <Scene
                    key="merchantdetail"
                    component={MerchantDetail}
                    hideNavBar={true}
                />
                <Scene
                    key="UserInfo"
                    component={UserInfo}
                    hideNavBar={true}
                />
                <Scene
                    key="UpdatePassword"
                    component ={UpdatePassword}
                    hideNavBar={true}
                />
                <Scene
                    key="HistoryDelivery"
                    component={HistoryDelivery}
                    hideNavBar={true}
                />
                <Scene
                    key="DetailHistory"
                    component={DetailHistory}
                    hideNavBar={true}
                />
                <Scene
                    key="tabbar"
                    tabs
                    hideNavBar={true}
                >
                    <Scene key="Home" title = {I18n.t('Home')} inittial component={Merchant}  hideNavBar={true} icon={myIconHome}/>
                    <Scene key="Map" title = {I18n.t('Map')} component = {Map} hideNavBar={true} icon={myIconMap}/>
                    <Scene key="Basket" title = {I18n.t('Basket')} component={Basket} hideNavBar={true} icon={myIconBasket}/>
                    <Scene key="Deliveryoder" title = {I18n.t('Deliveryorder')}component={DeliveryOrder} hideNavBar={true} icon={myIcondeliveryoder}/>
                    <Scene key="Setting" title = {I18n.t('Setting')} component={Setting} hideNavBar={true} icon={myIconSetting}/>
                </Scene>


            </Scene>
        </Router>


    );
  }
}
const mapStateToProps = (state) => ( {
    language:state.appReducer.language

})
const mapDispathToProps = (dispatch)=>({

})


export default connect(mapStateToProps,mapDispathToProps) (App)

