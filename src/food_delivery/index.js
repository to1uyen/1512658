/** @format */

import {AppRegistry} from 'react-native';
import { Provider } from 'react-redux'
import React from 'react'
import App from './App';
import {name as appName} from './app.json';

import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './src/Store';


const MyComponent = () => (
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <App/>
        </PersistGate>
    </Provider>
);

AppRegistry.registerComponent(appName, () => MyComponent);
